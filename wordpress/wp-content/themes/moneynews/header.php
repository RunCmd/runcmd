<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php endif; ?>

	<link rel="stylesheet" href="https://dhbhdrzi4tiry.cloudfront.net/cdn/sites/foundation.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/motion-ui/1.1.1/motion-ui.min.css">
	<link href='https://cdnjs.cloudflare.com/ajax/libs/foundicons/3.0.0/foundation-icons.css' rel='stylesheet' type='text/css'>
	<?php wp_head(); ?>
	

	<!--  Font awesome icons-->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">

</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<div class="site-inner">
		<header class="top-bar main-nav">
		<div class="title-bar" data-responsive-toggle="bar-menu" data-hide-for="medium">
		    <button class="menu-icon" type="button" data-toggle></button>
		    <div class="title-bar-title float-right"><?php twentysixteen_the_custom_logo(); ?></div>
		</div>
		  <div id="bar-menu">
		    <div class="row column text-center">
		      <div class="top-bar-left">
		        <ul class="menu logo-menu dropdown menu hide-for-small-only">
		        <li><?php twentysixteen_the_custom_logo(); ?>

		        </li>
		        </ul>
		        </div>
		      <div class="top-bar-right dropdown menu" data-dropdown-menu >
		      	<?php if ( has_nav_menu( 'primary' ) ) : ?>
						<nav id="site-navigation" class="main-navigation" role="navigation" aria-label="<?php esc_attr_e( 'Primary Menu', 'twentysixteen' ); ?>">
							<?php
								wp_nav_menu( array(
									'theme_location' => 'primary',
									'menu_class'     => 'menu menu-right',
								 ) );
							?>
						</nav><!-- .main-navigation -->
				<?php endif; ?>
		      </div>
		    </div>
		  </div>
		</header>
		<!--end Header -->

		<div id="content" class="site-content">
