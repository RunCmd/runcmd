<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>

		</div><!-- .site-content -->


		<!-- My Footer  -->
		<?php if ( is_active_sidebar( 'sidebar-2' ) ) : ?>
		<div class="widget-area">
			<?php dynamic_sidebar( 'sidebar-2' ); ?>
		</div><!-- .widget-area -->
	<?php endif; ?>
		
		<!-- My Footer END  -->
	</div><!-- .site-inner -->
</div><!-- .site -->

<script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>

<script src="https://dhbhdrzi4tiry.cloudfront.net/cdn/sites/foundation.js"></script>
<script>
      $(document).foundation();

</script>

<?php wp_footer(); ?>
</body>
</html>
